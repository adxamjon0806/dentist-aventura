const leftBtn = document.querySelector("#leftBtn"),
  rightBtn = document.querySelector("#rightBtn"),
  clientItem = document.querySelectorAll(".clients_list-item"),
  clientInfo = document.querySelectorAll(".clients_info"),
  navBtn = document.querySelector(".nav_btn"),
  navMobile = document.querySelector(".nav_mobile"),
  navMobLink = document.querySelectorAll(".nav_link-mobile");
let count = 0;
clientItem[count].classList.add("active"),
  clientInfo[count].classList.add("active"),
  leftBtn.addEventListener("click", () => {
    -1 == (count -= 1) && (count = clientItem.length - 1),
      clientItem.forEach((e) => {
        e.classList.remove("active");
      }),
      clientInfo.forEach((e) => {
        e.classList.remove("active");
      }),
      clientItem[count].classList.add("active"),
      clientInfo[count].classList.add("active");
  }),
  rightBtn.addEventListener("click", () => {
    (count += 1) == clientItem.length && (count = 0),
      clientItem.forEach((e) => {
        e.classList.remove("active");
      }),
      clientInfo.forEach((e) => {
        e.classList.remove("active");
      }),
      clientItem[count].classList.add("active"),
      clientInfo[count].classList.add("active");
  }),
  navBtn.addEventListener("click", () => {
    navBtn.classList.toggle("active"), navMobile.classList.toggle("active");
  }),
  navMobLink.forEach((e) => {
    e.addEventListener("click", () => {
      navBtn.classList.remove("active"), navMobile.classList.remove("active");
    });
  });
const minute = document.querySelector("#minute"),
  second = document.querySelector("#second");
let secTime = +second.innerHTML,
  minTime = +minute.innerHTML;
function Timer() {
  0 == (secTime -= 1) && 0 == minTime && location.reload(),
    0 == secTime &&
      ((secTime = 59), (minTime -= 1), (minute.innerHTML = minTime)),
    (second.innerHTML = secTime);
}
setInterval(Timer, 1e3);
